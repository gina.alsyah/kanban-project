import React, { useContext, useState } from "react";
import { DataContext } from "../context/store";
import delIcon from "../assets/delete.svg"
import "../sass/Card.scss"
import { Draggable } from "react-beautiful-dnd";

const Card = ({ idList, item, index }) => {
    const { changeCardDesc, deleteCard } = useContext(DataContext)
    const [edit, setEdit] = useState(false);
    const [text, setText] = useState(item.title);
    const isEdit = () => setEdit(true)
    const closeInput = (e) => {
        e.preventDefault()
        setEdit(false)
        changeCardDesc(idList, item.id, text)
    }
    const changeInput = (e) => {
        setText(e.target.value)
    }
    const delCard = () => {
        deleteCard(idList, item.id)
    }
    return (
        <Draggable draggableId={item.id} index={index}>
            {(provided) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    className="card-list"
                >
                    {edit ? (
                        <form onSubmit={closeInput}>
                            <input
                                autoFocus
                                type="text"
                                value={text}
                                onBlur={closeInput}
                                onChange={changeInput}
                            />
                        </form>
                    ) : (
                        <div className="card-list__text">
                            <p onClick={isEdit}>{item.title}</p>
                            <img src={delIcon} alt="delete" onClick={delCard} />
                        </div>
                    )}

                </div>
            )}
        </Draggable>
    )
}

export default Card