import React from "react";
import "../sass/Board.scss";
import BoardTitle from "./BoardTitle";
import Button from "./Button";
import menu from "../assets/menu.svg";
import Card from "./Card";
import { Droppable, Draggable } from "react-beautiful-dnd";

const Board = ({ data, index }) => {
    return (
        <Draggable draggableId={data.id} index={index}>
            {(provided) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    className="board"
                >
                    <div className="board__title">
                        <BoardTitle id={data.id} title={data.title} />
                        <div className="menu">
                            <img src={menu} alt="menu" />
                        </div>
                    </div>
                    <Droppable droppableId={data.id}>
                        {(provided) => (
                            <div ref={provided.innerRef} {...provided.droppableProps}>
                                {data.cards.map((card, idx) =>
                                    <Card key={card.id} item={card} idList={data.id} index={idx} />
                                )}
                                {provided.placeholder}
                            </div>
                        )}

                    </Droppable>
                        <Button id={data.id} />
                </div>
            )}
        </Draggable>
    )
}

export default Board