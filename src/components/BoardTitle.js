import React from "react";
import { useContext } from "react";
import { useState } from "react";
import { DataContext } from "../context/store";
import "../sass/BoardTitle.scss"

const BoardTitle = ({id, title}) => {
    const {changeTitle} = useContext(DataContext)
    const [text, setText] = useState(title)
    const [open, setOpen] = useState(false);
    const change = (e) => {
        setText(e.target.value)
    }
    const closeInput = (e) => {
        e.preventDefault()
        setOpen(false)
        changeTitle(id, text)
    }
    const openInput = () => {
        setOpen(true)
    }
    return(
        <div className="board-title">
            {open ? (
                <form onSubmit={closeInput}>
                    <input autoFocus type="text" value={text} onChange={change} onBlur={closeInput}/>
                </form>
            ) : (
                <h3 onClick={openInput}>{title}</h3>
            ) }
        </div>
    )
}

export default BoardTitle