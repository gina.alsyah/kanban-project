import React, { createContext, useState } from "react";
import { v4 as uuid } from "uuid";

const cards = [
    {
        id: "card-1",
        title: "learning how to code"
    },
    {
        id: "card-2",
        title: "reading a book"
    },
    {
        id: "card-3",
        title: "gym workout"
    }
]

const initialState = {
    lists: {
        "list-1": {
            id: "list-1",
            title: "Backlog",
            cards: cards
        },
        "list-2": {
            id: "list-2",
            title: "On Progress",
            cards: []
        }
    },
    listIds: ["list-1", "list-2"]
}

export const DataContext = createContext()

export const DataProvider = (props) => {
    const [store, setStore] = useState(initialState) 
    const changeTitle = (id, text) => {
        const item = store.lists[id]
        item.title = text
        const newStore = {
            ...store,
            lists: {
                ...store.lists,
                [id]: item
            }
        }
        setStore(newStore)
    }
    const changeCardDesc = (id, idcard, text) => {
        const itemCard = store.lists[id].cards.find(item => item.id === idcard)
        itemCard.title = text

        const item = store.lists[id]
        item.cards = [
            ...store.lists[id].cards,
        ]

        const newStore = {
            ...store,
            lists: {
                ...store.lists,
                [id]: item
            }
        }
        setStore(newStore)
    }
    const deleteCard = (id, idcard) => {
        const itemCard = store.lists[id].cards.filter(item => item.id !== idcard)

        const item = store.lists[id]
        item.cards = itemCard

        const newStore = {
            ...store,
            lists: {
                ...store.lists,
                [id]: item
            }
        }
        setStore(newStore)
    }
    const cardAdd = (listId, text) => {
        const item  = store.lists[listId]
        console.log("item", item)
        const id = uuid()
        const newCard = {
            id: `card-${id}`,
            title: text
        }
        item.cards = [...item.cards, newCard]
        const newStore = {
            ...store,
            lists: {
                ...store.lists,
                [listId] : item
            }
        }
        setStore(newStore)

    } 
    const listAdd = (text) => {
        const id = `list-${uuid()}`
        const newList = {
            id: id,
            title: text,
            cards: []
        }
        const newStore = {
            listIds: [...store.listIds, id],
            lists: {...store.lists, [id]: newList}
        }
        setStore(newStore)
    } 
    const updateDrag = (data) => {
        setStore(data)
    }
    return (
        <DataContext.Provider value={{ store, changeTitle, changeCardDesc, deleteCard, cardAdd, listAdd, updateDrag }}>
            {props.children}
        </DataContext.Provider>
    )
}